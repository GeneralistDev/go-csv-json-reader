package models

import (
	"godata/common"
	"strconv"
	"time"
)

// User struct representing a user record
type User struct {
	ID          int
	FirstName   string
	LastName    string
	Email       string
	DateOfBirth string
	SignupDate  time.Time
}

// NewUser creates from csv array string
func NewUser(fields []string) (*User, error) {
	var err error
	u := new(User)
	u.ID, err = strconv.Atoi(fields[0])
	if err != nil {
		return nil, err
	}

	u.FirstName = fields[1]
	u.LastName = fields[2]
	u.Email = fields[3]
	u.DateOfBirth = fields[4]
	u.SignupDate, err = time.Parse(common.SIMPLEDATE, fields[5])

	if err != nil {
		return nil, err
	}

	return u, nil
}

// ToStringArray returns an []string which can be fed directly to the CSV writer
func (u *User) ToStringArray() []string {
	return []string{strconv.Itoa(u.ID), u.FirstName, u.LastName, u.Email, u.DateOfBirth, u.SignupDate.Format(common.ISO8601)}
}
