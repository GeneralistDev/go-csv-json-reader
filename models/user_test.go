package models

import (
	"godata/common"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewUser(t *testing.T) {
	fields := []string{"0", "firstName", "lastName", "foo@email.com", "01/01/2020", "1/1/88,2016-07-11T20:49:23.385Z"}
	u, _ := NewUser(fields)

	assert.Equal(t, 0, u.Id, "Id is not '0'")
	assert.Equal(t, "firstName", u.FirstName, "Firstname is not 'firstName'")
	assert.Equal(t, "lastName", u.LastName, "Lastname is not 'lastName'")
	assert.Equal(t, "foo@email.com", u.Email, "Email is not equal to 'foo@email.com'")
	assert.Equal(t, "01/01/2020", u.DateOfBirth, "Date of Birth is not '01/01/2020'")
	parsedTime, _ := time.Parse(common.ISO8601, "1/1/88,2016-07-11T20:49:23.385Z")
	assert.Equal(t, parsedTime, u.SignupDate, "Signup date not equal")
}
