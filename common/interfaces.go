package common

type CsvLineParser interface {
	Parse()
}
