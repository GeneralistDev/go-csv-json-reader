package processing

import (
	"encoding/csv"
	"fmt"
	"godata/common"
	"godata/models"
	"io"
	"strings"
	"time"

	"github.com/bcicen/jstream"
)

// ParseCsv will parse a csv file
func ParseCsv(processQueue chan<- *models.User, failureQueue chan<- string, r *csv.Reader) {
	l, err := r.Read()
	for err != io.EOF {
		if err != nil {
			// We had an error fetching a record so we should log that
			failureQueue <- fmt.Sprintf("Failure when reading record: \"%s\" with error: %s", strings.Join(l[:], ","), err.Error())
		} else {
			u, userCreateError := models.NewUser(l)
			if userCreateError != nil {
				failureQueue <- fmt.Sprintf("Failure processing user record with error: %s and values: \"%s\"", userCreateError.Error(), strings.Join(l[:], ","))
			} else {
				processQueue <- u
			}
		}

		l, err = r.Read()
	}

	close(failureQueue)
	close(processQueue)
}

// ParseJSON will parse a json file
func ParseJSON(processQueue chan<- *models.User, failureQueue chan<- string, d *jstream.Decoder) {
	var err error

	for record := range d.Stream() {
		val := record.Value.(map[string]interface{})
		u := new(models.User)
		u.ID = int(val["Id"].(float64))
		u.FirstName = val["FirstName"].(string)
		u.LastName = val["LastName"].(string)
		u.Email = val["Email"].(string)
		u.DateOfBirth = val["DateOfBirth"].(string)
		u.SignupDate, err = time.Parse(common.SIMPLEDATE, val["SignupDate"].(string))

		if err != nil {
			failureQueue <- fmt.Sprintf("Failure when parsing user signup date: %s", err.Error())
		} else {
			processQueue <- u
		}
	}

	close(failureQueue)
	close(processQueue)
}
