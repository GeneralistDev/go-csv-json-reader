module godata

go 1.14

require (
	github.com/bcicen/jstream v1.0.0
	github.com/stretchr/testify v1.6.1
)
