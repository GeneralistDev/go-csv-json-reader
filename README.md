# README
This is a really simple IO learning example using Go.

## Features
### Basic IO
The goal here was not to load the entire input file into memory but rather to
stream them in record by record. That's easy 
    * Per record CSV reader
    * Per record JSON reader (streaming)
### Usage of channels and goroutines
The use of goroutines here is to concurrently process records while the file
is still being ingested. We achieve this by using a `processQueue` channel to
feed records to the processor goroutine.
