package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"godata/common"
	"godata/models"
	"godata/processing"
	"io"
	"os"
	"path/filepath"
	"sync"

	"github.com/bcicen/jstream"
)

func jsonProcessor(wg *sync.WaitGroup, processQueue chan<- *models.User, failureQueue chan<- string, f *os.File) {
	reader := bufio.NewReader(io.Reader(f))
	d := jstream.NewDecoder(reader, 1)

	processing.ParseJSON(processQueue, failureQueue, d)
	wg.Done()
}

func csvProcessor(wg *sync.WaitGroup, processQueue chan<- *models.User, failureQueue chan<- string, f *os.File) {
	reader := bufio.NewReader(io.Reader(f))
	r := csv.NewReader(reader)
	r.Comma = ','
	r.FieldsPerRecord = 6
	r.ReuseRecord = false
	r.TrailingComma = false

	processing.ParseCsv(processQueue, failureQueue, r)
	wg.Done()
}

func recordProcessor(wg *sync.WaitGroup, processQueue <-chan *models.User) {
	of, err := os.Create("output.csv")
	common.CheckError(err)
	writer := bufio.NewWriter(io.Writer(of))
	w := csv.NewWriter(writer)
	w.Comma = ','

	for user := range processQueue {
		w.Write(user.ToStringArray())
	}

	w.Flush()
	err = of.Close()
	common.CheckError(err)

	wg.Done()
}

func failedRecordProcessor(wg *sync.WaitGroup, errorQueue <-chan string) {
	of, err := os.Create("failed.csv")
	common.CheckError(err)

	writer := bufio.NewWriter(io.Writer(of))

	for line := range errorQueue {
		_, err = writer.WriteString(line)
		common.CheckError(err)
		writer.WriteString("\n")
	}

	writer.Flush()
	of.Close()

	wg.Done()
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("You must provide either a csv or json file to process")
		return
	}

	inputFileName := os.Args[1]

	f, err := os.Open(inputFileName)
	common.CheckError(err)

	processQueue := make(chan *models.User)
	failureQueue := make(chan string)

	wg := new(sync.WaitGroup)

	switch ext := filepath.Ext(inputFileName); ext {
	case ".json":
		fmt.Println("Processing JSON")
		go jsonProcessor(wg, processQueue, failureQueue, f)
		wg.Add(1)
	case ".csv":
		fmt.Println("Processing CSV")
		go csvProcessor(wg, processQueue, failureQueue, f)
		wg.Add(1)
	default:
		fmt.Println("File type not supported", ext)
	}

	go failedRecordProcessor(wg, failureQueue)
	wg.Add(1)

	go recordProcessor(wg, processQueue)
	wg.Add(1)

	wg.Wait()

	fmt.Println("Done")
}
